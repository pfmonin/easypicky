<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Company;
use App\Repository\CompanyRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{

    private $companyRepository;
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            // ->add('roles')
            ->add('roles', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices'  => [
                    'Admin' => 'ROLE_ADMIN',
                    'Client 1' => 'ROLE_CLIENT1',
                    'Client 2' => 'ROLE_CLIENT2',
                    'User' => 'ROLE_USER',
                ],
            ])
            ->add('password')
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'required' => false,
                'choice_label' => function(Company $company) {
                    return sprintf('(%d) %s', $company->getCompanyName(), $company->getCompanyName());
                },
                'placeholder' => 'Choose a company',
                'choices' => $this->companyRepository->findAll(),
            ])
        ;


        $builder->get('roles')
        ->addModelTransformer(new CallbackTransformer(
            function ($rolesArray) {
                // transform the array to a string
                return count($rolesArray)? $rolesArray[0]: null;
            },
            function ($rolesString) {
                // transform the string back to an array
                return [$rolesString];
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
