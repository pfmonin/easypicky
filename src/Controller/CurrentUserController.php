<?php




class CurrentUserController 
{
    private $security = null;

    public function __construct( Security $security ) {}

    public function __invoke() {

        $user = $this->security->getUser();
        return $user;
    }

}