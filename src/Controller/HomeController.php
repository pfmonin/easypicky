<?php

namespace App\Controller;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(CompanyRepository $repo): Response
    {
        $companies = $repo->findAll();
        

        return $this->render('home/index.html.twig', [
            'companies' => $companies,

        ]);
    }
}
