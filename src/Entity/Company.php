<?php

namespace App\Entity;


use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Security\Voter\CompanyVoter;
use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\DependencyInjection\Loader\Configurator\security;



#[ORM\Entity(repositoryClass: CompanyRepository::class)]

#[ApiResource(
    security: "is_granted('ROLE_USER')",
    normalizationContext: ['groups' => ['read:collection']],
    denormalizationContext: ['groups' => ['write:collection']],  
    collectionOperations: [
        'get' => [
            'method' => 'GET',
            'path' => '/companies',
            'controller' => CompanyVoter::class,            
            'normalization_context' => [ 'groups' => ['adminread:collection'] ],
            'security' => 'is_granted("COMPANY_READCOLLECTION")',
            'securityMessage' => 'Only admin can see all companies !',
        ],
       'post' => [
            'method' => 'POST',
            'path' => '/companies',
            'controller' => CompanyVoter::class,            
            'normalization_context' => [ 'groups' => ['adminpost:collection'] ],
            'security' => 'is_granted("COMPANY_CREATE", , object)',
            'securityMessage' => 'Only admin can create a company !',
       ], 

    ],
    
    itemOperations: [
        'get' => [
            'method' => 'GET',
            'path' => '/companies/{id}',
        ], 
        'client1Get' => [
            'method' => 'GET',
            'path' => '/companies/your-company/{id}',
            'controller' => CompanyVoter::class,            
            'normalization_context' => ['client1read:item'],
            'security' => "is_granted('COMPANY_CLIENT1_READITEM', object)",
            'securityMessage' => 'Only one specific client (1) can see that !'
        ],
        'client2Get' => [
            'method' => 'GET',
            'path' => '/companies/your-company/{id}',
            'controller' => CompanyVoter::class,            
            'normalization_context' => ['client2read:item'],
            'security' => "is_granted('COMPANY_CLIENT2_READITEM', object)",
            'securityMessage' => 'Only one specific client (1) can see that !'
        ],
        'client1Edit' => [
            'method' => 'PUT',
            'path' => '/companies/your-company/{id}',
            'controller' => CompanyVoter::class,            
            'denormalization_context' => [ 'groups' => ['client1put:item'] ],
            'security' => "is_granted('COMPANY_CLIENT1_PUTITEM', object)",
        ],
        'client2Edit' => [
            'method' => 'PUT',
            'path' => '/companies/your-company/{id}',
            'controller' => CompanyVoter::class,            
            'denormalization_context' => [ 'groups' => ['client2put:item'] ],
            'security' => "is_granted('COMPANY_CLIENT2_PUTITEM', object)",
        ]
        
    ],
)]




class Company
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]   
    private ?int $id = null; // ! Ne doit jamais être lu par l'api



    
    #[ORM\Column(length: 255)]
    #[Groups(['client1put:item' , 'client1read:item', 'client2put:item' ,'client2read:item'])]
    private ?string $companyName = null;



   
    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['client1put:item' , 'client1read:item', 'client2put:item' ,'client2read:item'])]
    private ?string $activityArea = null;



    
    #[ORM\Column(length: 255)]  
    private ?string $siret = null; // ! Ne doit jamais être lu par l'api



      
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['client1put:item' , 'client1read:item'])]
    private ?string $address = null;



    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['client1put:item' , 'client1read:item'])]
    private ?string $zipcode = null;

    
    #[ORM\Column(length: 255)]
    #[Groups(['client1put:item' , 'client1read:item'])]
    private ?string $city = null;

    
    #[ORM\OneToMany(mappedBy: 'company', targetEntity: User::class)]
    private Collection $users; // ! ne doit pas etre lu par l'api

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getActivityArea(): ?string
    {
        return $this->activityArea;
    }

    public function setActivityArea(string $activityArea): self
    {
        $this->activityArea = $activityArea;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCompany() === $this) {
                $user->setCompany(null);
            }
        }

        return $this;
    }

    

}
