<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CompanyVoter extends Voter
{
    public const VIEW   = 'COMPANY_READCOLLECTION';
    public const POST   = 'COMPANY_CREATE';
    public const DELETE = 'COMPANY_DELETEITEM';

    public const VIEWC1 = 'COMPANY_CLIENT1_READITEM';
    public const VIEWC2 = 'COMPANY_CLIENT2_READITEM';

    public const PUTC1  = 'COMPANY_CLIENT1_PUTITEM';
    public const PUTC2  = 'COMPANY_CLIENT2_PUTITEM';


    public function __construct(private Security $security) {
        $this->security = $security;
    }





    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::POST, 
            self::VIEW,
            self::DELETE, 
            self::VIEWC1,
            self::PUTC1, 
            self::VIEWC2,
            self::PUTC2           
        ])
        && $subject instanceof Company;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        $hasAccessClient1 = $this->security->isGranted('ROLE_CLIENT1');
        $hasAccessClient2 = $this->security->isGranted('ROLE_CLIENT2');

        switch ($attribute) {
            case self::POST:
                if ($this->security->isGranted(Role::ADMIN) ) { return true; }             
                break;
            case self::VIEW:
                if ($this->security->isGranted(Role::ADMIN) ) { return true; }               
                break;
            case self::DELETE:
                if ($this->security->isGranted(Role::ADMIN) ) { return true; }               
                break;

            case self::VIEWC1:
                if ($hasAccessClient1) { return true; }             
                break;
            case self::PUTC1:
                if ($hasAccessClient1) { return true; }             
                break;

            case self::VIEWC2:
                if ($hasAccessClient2 ) { return true; }               
                break;           
            case self::PUTC2:
                if ($hasAccessClient2 ) { return true; }               
                break;
        }       

        return false;
    }


    private function canView(Company $company, User $user): bool
    {
        if ($this->canEdit($company, $user)) {
            return true;
        }
    }

    private function canEdit(Company $company, User $user): bool
    {
        return $user === $post->getCompany();
    }

}
