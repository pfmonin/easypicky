#Easypicky Test technique
Développement d'un dashobard Admin et d'une api pour les users.

Svp veuillez prendre en compte que je travaille avec api plateform pour la 1ère fois. 

## Configuration 
symfony v6
php8

## Installation 
La base de donnée se trouve dans a la racine du projet dans le dossier BDD.
Elle contient : 

    - Une table user avec comme données 1 admin et 2 users 

    - Une table company avec comme données 2 compagnies

#### Pour installer le projet et ses dépendances
cloner le projet
run composer install pour installer les dépendances

### Installer la bdd 
Dans le .env remplacer les accès à la bdd par vos accès

run php bin/console d:d:c pour créer la bdd
importer les données manuellement (je n'ai pas crée de fixtures)

run symfony serve ou php bin/console server:run pour lancer le server

### Les accès utilisateurs et admin 
admin: 
email : admin@easypicky.fr
password: 12345678

Client 1: 
email : client1@easypicky.fr
password: 12345678

Client 2: 
email : client2@easypicky.fr
password: 12345678

### Les routes
https://localhost:8000/ => home 
https://localhost:8000/login => Login 
https://localhost:8000/api => API Plateform
https://localhost:8000/api/login => Login API 



### Fonctionnalités: 
Entité

- ✅ 1. Créer une entité User afin de créer des utilisateurs (firstName, lastName, Email, Password, etc).
- ✅ 2. Créer une entité Company (name, siren, activityArea, Address, cp, city, country, etc).

Fonctionnement
- ✅ 1. Créer trois utilisateurs, 1 Admin et 2 clients. Chaque client sera lié à une compagnie différente (Etc : Danone, Mondelez).
- ✅ 2. Créer un crud permettant d’ajouter, éditer, supprimer un utilisateur. 
- ✅ 3. Créer un crud permettant d’ajouter, éditer, supprimer une compagnie.
- ✅ A la création d’un utilisateur, permettre de lui associer un rôle (admin, etc) et si l’utilisateur est un client l’associer à une compagnie.
- ✅ 1. Pas besoin de faire une jolie interface pour les crud, un twig basic est largement suffisant.
- ✅ 2. Pour la création de l’APi vous êtes libre d’utiliser la solution de votre choix.
- ✅ 3. Seul l’administrateur pourra se connecter à l’interface d’administration avec les crud.
- ✅ 4. Les clients devront être authentifiés afin de pouvoir faire les appels API.

Interface
- 4. Permettre aux clients de faire des appels API afin de récupérer des infos sur la compagnie qui leur est associée :
- 5. Permettre aux clients d’éditer les données de leur compagnie par appel API.
- 5. Les clients ne pourront éditer par API que les champs qu’ils ont le droit de voir à la récupération
-- Pour cette partie la je n'ai pas reussi a verrouiller les routes concernant les compagnies n'appartenant pas l'utilisateur connecté. Malgré le système de ROLE_ . Mon controller custom n'est pas lisible par l'api. Je ne comprends pas pourquoi. 


- Le client 1 devra récupérer toutes les données à l’exception de l’id et du siren.
- Le client 2 ne récupérera que le nom et l’activityArea.









